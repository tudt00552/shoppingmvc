<%-- 
    Document   : cart
    Created on : Jun 4, 2019, 2:09:11 PM
    Author     : Setup
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="prod" class="com.shopping.model.ProductCart" scope="session"/>

    <center>
        <table bgcolor="#FFFcc" border="1" cellspacing="0" cellpadding="0">
            <tr>
                <td><b> Product Id </b></td>
                <td><b> Product Name </b></td>
                <td><b> Product Type </b></td>
                <td><b> Price </b></td>
                <td><b> Quantity </b></td>
            </tr>
            <c:forEach var="item" items="${prod.cartItems}">
                <tr>
                    <td>${item.productId}</td>
                    <td>${item.productName}</td>
                    <td>${item.productType}</td>
                    <td>${item.price}</td>
                    <td>${item.quantity}</td>
                    <td>
                        <form action="ShoppingServlet" name="deleteForm" method="POST">
                            <input type="submit" value="Delete"/>
                            <input type="hidden" name="delItem" value="${item.productId}"/>
                            <input type="hidden" name="action" value="DELETE"/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <form name="checkOutForm" action="ShoppingServlet" method="POST">
            <input type="hidden" name="action" value="CHECKOUT"/>
            <input type="submit" name="Checkout" value="Checkout"/>
        </form>
    </center>

</body>
</html>
